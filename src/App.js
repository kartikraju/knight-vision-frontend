import { Suspense, lazy } from 'react';
import { Routes, Route, HashRouter } from 'react-router-dom';
import { LiveDataViewer } from './components/LiveDataViewer';
import LiveDataPage from './components/LiveDataPage';
import BaseConfig from './components/BaseConfig';
import NavBar from './components/NavBar';
import logo from './logo.svg';
import './App.css';

const TestComponent = () => {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

function App() {
  return (
    <HashRouter>
      <Suspense fallback={<><div style={{textAlign: 'center'}}>Loading...</div></>}>
      <Routes>
        <Route path="" element={<><NavBar/><LiveDataPage/></>}/>
        <Route path="/base-config" element={<><NavBar/><BaseConfig/></>}/>
        <Route path="/warranty" element={<><NavBar/><TestComponent/></>}/>
        <Route path="/live" element={<><NavBar/><LiveDataViewer/></>} />
      </Routes>
      </Suspense>
    </HashRouter>
  );
}

export default App;
