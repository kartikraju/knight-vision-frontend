import React from 'react';
import { SocketContext } from './SocketContext';
import { initSocket, getSocket } from './socket';

initSocket();
// const socket = getSocket();

export const SocketProvider = ({ children }) => {
  const [mydata, setMydata] = React.useState({});
  const [connected, setConnected] = React.useState(false);
  const [error, setError] = React.useState('');
  const [state, setState] = React.useState('PASSIVE');
  const [jsonData, setJsonData] = React.useState({});
  const [socket, setSocket] = React.useState(getSocket());

  const mergeListObjects = (listOfObjects) => {
    const mergedObject = {};

    listOfObjects.forEach((obj) => {
      for (const key in obj) {
        if (typeof obj[key] === "object" && obj[key] !== null) {
          if (!mergedObject[key]) {
            // If the property doesn't exist in the merged object, initialize it
            mergedObject[key] = Array.isArray(obj[key]) ? [] : {};
          }
          // Recursively merge the nested object or list
          mergedObject[key] = mergeListObjects([mergedObject[key], obj[key]]);
        } else {
          // Otherwise, assign the value directly
          mergedObject[key] = obj[key];
        }
      }
    });

    return mergedObject;
  }

  React.useEffect(() => {
    setSocket(getSocket());
  }, []);

  React.useEffect(() => {

    const onConnect = () => {
      setConnected(true);
    }
    const onDisconnect = () => {
      setConnected(false);
    }
    const onDataEvent = (value) => {
      setMydata({...value});
      const data = mergeListObjects(Object.keys(value).map(item => value[item].json_data))
      setJsonData(data);
    }
    const onErrorEvent = (value) => {
      setError(value);
    }
    const onStateEvent = (value) => {
      setState(value);
    }

    socket.on('connect', onConnect);
    socket.on('disconnect', onDisconnect);
    socket.on('data', onDataEvent);
    socket.on('error', onErrorEvent);
    socket.on('state', onStateEvent);

    return () => {
      socket.off('connect', onConnect);
      socket.off('disconnect', onDisconnect);
      socket.off('data', onDataEvent);
    };
  }, []);

  return (
    <SocketContext.Provider value={{socket, mydata, jsonData, connected, error, state}}>
      {children}
    </SocketContext.Provider>
  );
};