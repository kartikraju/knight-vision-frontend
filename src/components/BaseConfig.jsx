import React from "react";
import { Button } from "react-bootstrap";
import { SocketContext } from '../SocketContext';

const BaseConfig = () => {
  const {socket} = React.useContext(SocketContext);

  const handleOnClick = () => {
    console.log('button clicked')
    socket.emit('get_config', 'hi')
  }
  return (
    <>
      <Button onClick={handleOnClick} >Get Config</Button>
    </>
  );
}

export default BaseConfig;