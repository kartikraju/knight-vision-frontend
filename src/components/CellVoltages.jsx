import React, { useContext } from "react";
import ReactTable from './Table';
import { SocketContext } from '../SocketContext';
import { useTable, useSortBy, useGroupBy, useRowSelect, useExpanded, useGlobalFilter } from 'react-table';

const CellVoltages = () => {
  const {jsonData, connected, error, state} = useContext(SocketContext);
  
  const tableData = React.useMemo(() => {
    const keys = Object.keys(jsonData?.table3? jsonData?.table3: {})
    const data = keys? keys.map((item, idx) => (
      {
        name: item.substring(11),
        value: jsonData.table3[item].toFixed(3),
        balancing: (jsonData['active_cell_balancing_status'] >> idx) & 1
      }
      )): []
    return data},
  [jsonData]);

  var tableColumns = [
    {
      Header: 'Cell ID',
      accessor: 'name',
      visible: 1,
      Cell: ({row}) => row.values['name'],
    },
    {
      Header: 'Value',
      accessor: 'value',
      visible: 1,
      Cell: ({row}) => row.values['value'],
    },
    {
      Header: 'Balancing',
      accessor: 'balancing',
      visible: 1,
      Cell: ({row}) => row.values['balancing']? <span style={{color: 'green'}}>ON</span>: <span style={{color: 'gray'}}>OFF</span>,
    },
  ]

  const columns = React.useMemo(() => tableColumns, []);

  const tableInstance = useTable(
    { columns: columns,
      data: tableData,
      manualPagination: true,
      manualSortBy: true,
      manualGlobalFilter: true,
      autoResetSelectedRows: true,
      autoResetPage: false,
    },
    useGlobalFilter,
    useGroupBy,
    useSortBy,
    useExpanded,
    useRowSelect,
  );

  return (
    <div style={{display: 'flex'}}>
      <div style={{flex: '1 1 auto'}}>
        <span>Cell Voltages</span>
        <ReactTable tableProps={tableInstance} loading={state.loadingData}/>
      </div>
    </div>
  );
}

export default CellVoltages;