import { useContext } from "react";
import NavBar from "./NavBar";
import ProtectionWarningList from "./ProtectionWarningList";
import LiveDataTable from "./LiveDataTable";
import TemperaturesTable from "./Temperatures";
import CellVoltages from "./CellVoltages";
import HardwareErrors from "./HardwareErrors";
import StatusList from "./StatusList";

const LiveDataPage = () => {

  return (
    <>
      <NavBar/>
      <div style={{margin: '0.5% 0.5% 0.5% 0.5%', display: 'flex'}}>
        <div style={{width: 'auto', padding: '5px'}}>
          <LiveDataTable/>
        </div>

        <div style={{width: 'auto', padding: '5px'}}>
          <TemperaturesTable/>
        </div>

        <div style={{width: 'auto', padding: '5px'}}>
          <CellVoltages/>
        </div>

        <div style={{width: 'auto', padding: '5px'}}>
          <ProtectionWarningList/>
        </div>

        <div style={{width: 'auto', padding: '5px'}}>
          <HardwareErrors/>
        </div>

        <div style={{width: 'auto', padding: '5px'}}>
          <StatusList/>
        </div>

      </div>
    </>
  );
}

export default LiveDataPage;
