import Table from 'react-bootstrap/Table';
import React from 'react';
import { TiArrowUnsorted, TiArrowSortedUp, TiArrowSortedDown } from 'react-icons/ti';

const ReactTable = (props) => {
  const {
    getTableProps,
    headerGroups,
    getTableBodyProps,
    rows,
    prepareRow,
    visibleColumns,
    getToggleAllRowsExpandedProps,
    isAllRowsExpanded,
    state: { groupBy, expanded },
  } = props.tableProps;

  return (
    <Table hover size="sm" {...getTableProps({style: {whiteSpace: 'nowrap', overflow: 'auto'}})}>
      <thead>
        {headerGroups.map(headerGroup => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map(column => (
              <th {...column.getHeaderProps()}>
                {column.render('Header')}
              </th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
      {rows.length
        ? React.Children.toArray(
          rows.map((row, i) => {
            prepareRow(row)
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map(cell => {
                  return (
                    <td
                      // For educational purposes, let's color the
                      // cell depending on what type it is given
                      // from the useGroupBy hook
                      {...cell.getCellProps()}
                      style={{
                        background: cell.isGrouped
                          ? '#0aff0082'
                          : cell.isAggregated
                          ? '#ffa50078'
                          : cell.isPlaceholder
                          ? '#ff000042'
                          : 'white',
                      }}
                    >
                      {cell.render('Cell')}
                    </td>
                  )
                })}
              </tr>
            )
          })
        ) : <tr>
            <td colSpan={props.tableProps.columns.length+1}
             style={{textAlign: 'center', color: 'rgba(0, 0, 0, 0.38)'}}
            >
              {props.loading? 'Loading items...': 'No data available'}
            </td>
          </tr>
      }
      </tbody>
    </Table>
  );
}

export default ReactTable;