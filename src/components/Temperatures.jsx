import React, { useContext } from "react";
import ReactTable from './Table';
import { SocketContext } from '../SocketContext';
import { useTable, useSortBy, useGroupBy, useRowSelect, useExpanded, useGlobalFilter } from 'react-table';

const TemperaturesTable = () => {
  const {jsonData, connected, error, state} = useContext(SocketContext);
  
  const tableData = React.useMemo(() => {
    const keys = Object.keys(jsonData?.table2? jsonData?.table2: {})
    const data = keys? keys.map(item => (
      {name: item.substring(15), value: jsonData.table2[item]}
      )): []
    return data},
  [jsonData]);

  var tableColumns = [
    {
      Header: 'Sensor ID',
      accessor: 'name',
      visible: 1,
      Cell: ({row}) => row.values['name'],
    },
    {
      Header: 'Value',
      accessor: 'value',
      visible: 1,
      Cell: ({row}) => row.values['value'],
    },
  ]

  const columns = React.useMemo(() => tableColumns, []);

  const tableInstance = useTable(
    { columns: columns,
      data: tableData,
      manualPagination: true,
      manualSortBy: true,
      manualGlobalFilter: true,
      autoResetSelectedRows: true,
      autoResetPage: false,
    },
    useGlobalFilter,
    useGroupBy,
    useSortBy,
    useExpanded,
    useRowSelect,
  );

  return (
    <div style={{display: 'flex'}}>
      <div style={{flex: '1 1 auto'}}>
        <span>Temperatures</span>
        <ReactTable tableProps={tableInstance} loading={state.loadingData}/>
      </div>
    </div>
  );
}

export default TemperaturesTable;