/**
* main page object containing all methods, selectors and functionality
* that is shared across all page objects
*/

class HomePage {
  open () {
    return browser.url(`http://localhost:3000`)
  }
}
module.exports = new HomePage();